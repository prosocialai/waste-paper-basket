# Waste Paper Basket

## What is it?

Waste Paper Basket (wpb) is a safe and sane implementation of GNU
coreutils `rm` command inspired by the idea behind [Trashy by Klaatu](https://gitlab.com/trashy/trashy) and the code behind the
[uutils project's rust implementation of the GNU coreutils](https://github.com/uutils/coreutils)

## Why is it?

`rm` is both unsafe for someone who wants to keep data and someone who
wants to remove it. Waste Paper Basket aims to fix this by offering the
features and options of `rm`, while allowing it to both operate as a
sane trashcan as well as a safe way of removing files by passing it
through a process like `shred`

## Why Rust?

I am using rust because it is sane, easily readable, and easily
maintainable. I encourage you to look at my code and the comments in it
to get a better feel for the language and programming in general.

The [tl;dr](https://tldr.sh) version of my stance on programming
languages, after using many in the last 20 years, is that they should be
easy to learn, easy to read, easy to understand, and,
**most** importantly, should **never** abstract away the fact that you are
using a machine.

For more information on my views on rust, listen to my episode on HPR
[Rust 101 - Episode 0: What in tarnishing?](https://hackerpublicradio.org/eps.php?id=3426).

# Current Features

  - Trashes files
  - Trashes empty directories (-d)
  - Trashes directories recursively (-r)
  - Can be forced to cooperate (-f)
  - Can prompt always (-i), once (-I), never (default) or any WHEN you want (--interactive=WHEN)
  - Passthrough to `rm` (-p)
  - Undo trashings using regex on trashinfo files (-u)
  - Empty trash can (-e)
  - Choose to use system trash (requires root) (--system)

# To-Do List

  - Compatibility for all other `rm` flags\!
  - Passthrough to `shred` (-s)

### Latest Release Versions


### Contact Info

**Email:** [izzy leibowitz at pm dot me](izzyleibowitz@pm.me.html)

**Mastodon:** [at blackernel at nixnet dot social](https://nixnet.social/BlacKernel)
