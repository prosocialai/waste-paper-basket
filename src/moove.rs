use crate::parse;
use crate::atomic_move::atomic_move;
use crate::clob_count::clob_count;

use std::path::{Path, PathBuf};

pub fn moove(options: parse::Options, mut mut_options: parse::MutOptions) {
    for file in &options.files {
        let file_path = Path::new(&file);
        let dest_dir = Path::new(&options.dest_dir);
    
        let ccount = clob_count(&file_path, &dest_dir, &options);

        let mut fixed_file = Path::new("");
        let mut fixed_file_pathbuf = PathBuf::new();

        if ccount == 0 { fixed_file = file_path }
        else
        {
            let str_ccount = format!("{}", ccount);
            let fixed_file_name = [ str_ccount, ".".to_string() , file.to_string() ].concat();

            fixed_file_pathbuf = dest_dir.join(&fixed_file_name);
            fixed_file = &fixed_file_pathbuf;

        }
        
        mut_options.dest_dir = dest_dir.display().to_string();
        
        let dest = [&mut_options.dest_dir.clone(), "/", fixed_file.file_name().unwrap().to_str().unwrap()].concat();
        let dest_path = Path::new(&dest);
 
        if options.verbose { println!("attempting to move {}", file) }

        match atomic_move(&file_path, &dest_path, &options, &mut mut_options) {
            true => { if !options.force { eprintln!("failed to move {}", file) } },
            false => { if options.verbose { println!("moved {} successfully", file) } },
        }
    }
}
