
use clap;

#[derive(Eq, PartialEq, Clone, Copy, Debug)]
pub enum InteractiveMode {
    None,
    Once,
    Always,
}

#[derive(Debug)]
pub struct Options {
    pub interactive: InteractiveMode,
    pub mode_message: String,
    #[allow(dead_code)]
    pub one_fs: bool,
    pub force: bool,
    pub preserve_root: bool,
    pub recursive: bool,
    pub dir: bool,
    pub verbose: bool,
    pub passthrough: bool,
    pub shred: bool,
    pub undo: bool,
    pub empty: bool,
    pub list: bool,
    pub system: bool,
    pub moove: bool,
    pub dest_dir: String,
    pub ovum: bool,
    pub files: Vec<String>,
}

pub struct MutOptions {
    pub dest_dir: String,
    pub cwd: String,
    pub recur_dest: String,
}

static ABOUT: &str                  = "Trash FILE(s) in accordence with FreeDesktop spec";

// Original rm options
static OPT_DIR: &str                = "dir";
static OPT_INTERACTIVE: &str        = "interactive";
static OPT_I_LOWER: &str            = "i";                      // NOTE: -i is the same as --interactive always
static OPT_I_UPPER: &str            = "I";                      // NOTE: -I is the same as --interactive once
static OPT_FORCE: &str              = "force";
static OPT_NO_PRESERVE_ROOT: &str   = "no-preserve-root";
static OPT_PRESERVE_ROOT: &str      = "preserve-root";          // NOTE: Since this is the default, we only need to check if no-preserve-root is set
static OPT_ONE_FILE_SYSTEM: &str    = "one-file-system";
static OPT_RECURSIVE: &str          = "recursive";
static OPT_R_UPPER: &str            = "R";                      // NOTE: -R is the same as --recursive, but clap does weird things with aliases
static OPT_VERBOSE: &str            = "verbose";

// Waste Paper Basket original options
static OPT_PASSTHROUGH: &str        = "passthrough";            // NOTE: Removes rather than trashes. Prompts always. (Like rm -i)
static OPT_SHRED: &str              = "shred";                  // NOTE: Shreds rather than trashes. Prompts always.
static OPT_UNDO: &str               = "undo";                   // NOTE: Recovers the FILE(s)
static OPT_EMPTY: &str              = "empty";                  // NOTE: Empties the trash
static OPT_LIST: &str               = "list";                   // NOTE: Lists the contents of the trash dir or given subdirectory
static OPT_MOOVE: &str              = "move";                   // NOTE: Moves a file to a given directory instead of the trash
static OPT_SYSTEM: &str             = "system";                 // NOTE: Uses the system instead of user trash. Requires root.

static OPT_OVUM: &str               = "ovum";

// Arguments
static ARG_FILES: &str              = "files";

fn get_usage() -> String {
    format!("wpb [OPTION]... FILE...")
}

fn get_long_usage() -> String {
    String::from("
        By default, wpb does not trash directories. Use the --recursive (-r or -R)
        option to trash each listed directory, too, along with all of its contents.

        To trash a file whose name starts with a '-', for example '-foo',
        use one of these commands:
        wpb -- -foo

        wpb ./-foo

        Note that if you use wpb to trash a file, it is possible to recover the file
        along with all of its contents by using the --undo (-u) option or by going to
        $HOME/.local/share/Trash for users and /.Trash for administrators.

        If you wish to remove files, rather than trashing them, use the --passthrough (-p)
        option to remove (unlink) files and the --shred (-q) option to overwrite them with
        random bits from /dev/urandom.
        ",
    )
}

fn get_app() -> clap::App<'static, 'static> {
    clap::App::new("waste-paper-basket")
        .version(clap::crate_version!())
        .about(ABOUT)

        // rm original options
        .arg(
            clap::Arg::with_name(OPT_INTERACTIVE)
            .long(OPT_INTERACTIVE)
            .help("prompt according to WHEN: never, once (-I), or always (-i). Without WHEN, prompt always")
            .value_name("WHEN")
            .takes_value(true)
        )
        .arg(
            clap::Arg::with_name(OPT_I_UPPER)
            .short(OPT_I_UPPER)
            .help("prompt once before trashing more than three files, or when trashing recursively. Less intrusive than -i, while still giving some protection against most mistakes")
        )
        .arg(
            clap::Arg::with_name(OPT_I_LOWER)
            .short(OPT_I_LOWER)
            .help("prompt before every trash")
        )

        .arg(
            clap::Arg::with_name(OPT_RECURSIVE)
            .long(OPT_RECURSIVE)
            .short("r")
            .help("trash directories and their contents recursively")
        )
        .arg( // NOTE: See above in statics why this is necissary
            clap::Arg::with_name(OPT_R_UPPER)
            .short(OPT_R_UPPER)
            .help("equivilant to -r")
        )

        
        .arg(
            clap::Arg::with_name(OPT_ONE_FILE_SYSTEM)
            .long(OPT_ONE_FILE_SYSTEM)
            .help("when trashing a hierarchy recursively, skip any directory that is on a file system different from that of the corresponding command line argument (NOT IMPLEMENTED)")
        )
        .arg(
            clap::Arg::with_name(OPT_NO_PRESERVE_ROOT)
            .long(OPT_NO_PRESERVE_ROOT)
            .help("do not treat '/' specially")
        )
        .arg(
            clap::Arg::with_name(OPT_PRESERVE_ROOT)
            .long(OPT_PRESERVE_ROOT)
            .help("do not trash '/' (default)")
        )

        .arg(
            clap::Arg::with_name(OPT_DIR)
            .long(OPT_DIR)
            .short("d")
            .help("trash empty directories")
        )
        .arg(
            clap::Arg::with_name(OPT_VERBOSE)
            .long(OPT_VERBOSE)
            .short("v")
            .help("explain what is being done")
        )
        .arg(
            clap::Arg::with_name(OPT_FORCE)
            .long(OPT_FORCE)
            .short("f")
            .help("ignore nonexistent files and arguments, never prompt")
            .multiple(true)
        )

        // Waste Paper Basket original options
        .arg(
            clap::Arg::with_name(OPT_UNDO)
            .long(OPT_UNDO)
            .short("u")
            .help("undo (recover) the trashed files")
            .conflicts_with_all(&[OPT_EMPTY, OPT_LIST, OPT_PASSTHROUGH, OPT_SHRED, OPT_MOOVE, OPT_OVUM])
        )
        .arg(
            clap::Arg::with_name(OPT_EMPTY)
            .long(OPT_EMPTY)
            .short("e")
            .help("empty the trash")
            .conflicts_with_all(&[OPT_LIST, OPT_PASSTHROUGH, OPT_SHRED, OPT_MOOVE, OPT_OVUM])
        )
        .arg(
            clap::Arg::with_name(OPT_LIST)
            .long(OPT_LIST)
            .short("l")
            .help("lists the trash dir or given subdirectory")
            .conflicts_with_all(&[OPT_PASSTHROUGH, OPT_SHRED, OPT_MOOVE, OPT_OVUM])
        )
        .arg(
            clap::Arg::with_name(OPT_PASSTHROUGH)
            .long(OPT_PASSTHROUGH)
            .short("p")
            .help("passthrough to rm (remove the files); prompt always")
            .conflicts_with_all(&[OPT_SHRED, OPT_MOOVE, OPT_OVUM, OPT_SYSTEM])
        )
        .arg(
            clap::Arg::with_name(OPT_SHRED)
            .long(OPT_SHRED)
            .short("q")
            .help("qassthrough to shred (overwrite the files); prompt always")
            .conflicts_with_all(&[OPT_MOOVE, OPT_OVUM, OPT_SYSTEM])
        )
        .arg(
            clap::Arg::with_name(OPT_SYSTEM)
            .long(OPT_SYSTEM)
            .short("s")
            .help("system trash instead of user trash (requires root)")
        )
        .arg(
            clap::Arg::with_name(OPT_MOOVE)
            .long(OPT_MOOVE)
            .short("m")
            .help("move a file to DEST_DIR instead of trash")
            .value_name("DEST_DIR")
            .takes_value(true)
            .conflicts_with(OPT_OVUM)
        )

        .arg(
            clap::Arg::with_name(OPT_OVUM)
            .short("δ")
            .long("xyzzy")
            .hidden(true)
        )

        // Files argument
        .arg(
            clap::Arg::with_name(ARG_FILES)
            .multiple(true)
            .takes_value(true)
            .min_values(1)
        )
}

pub fn parse_options() -> Options {
    let usage = get_usage();
    let long_usage = get_long_usage();

    let matches = get_app()
        .usage(&usage[..])
        .after_help(&long_usage[..])
        .get_matches();

    let options = Options {
        interactive: {
            if matches.is_present(OPT_I_LOWER) || (matches.is_present(OPT_PASSTHROUGH) || matches.is_present(OPT_SHRED)) {
                InteractiveMode::Always
            } else if matches.is_present(OPT_I_UPPER) {
                InteractiveMode::Once
            } else if matches.is_present(OPT_INTERACTIVE) {
                match matches.value_of(OPT_INTERACTIVE).unwrap() {
                    "never"  | "no" | "none" => InteractiveMode::None,
                                      "once" => InteractiveMode::Once,
                    "always" | "yes"         => InteractiveMode::Always,
                    val                      => { eprintln!("wpb: invalid argument to --interactive ({})", val); std::process::exit(1) },
                }
            } else { InteractiveMode::None }
        },

        mode_message: {
            if matches.is_present(OPT_UNDO) {
                "undo".to_string()
            } else if matches.is_present(OPT_PASSTHROUGH) {
                "remove".to_string()
            } else if matches.is_present(OPT_EMPTY) {
                "empty".to_string()
            } else if matches.is_present(OPT_LIST) {
                "list".to_string()
            } else if matches.is_present(OPT_MOOVE) {
                "move".to_string()
            } else {
                "trash".to_string()
            }
        },

        force: matches.is_present(OPT_FORCE),
        one_fs: matches.is_present(OPT_ONE_FILE_SYSTEM),
        preserve_root: !matches.is_present(OPT_NO_PRESERVE_ROOT),
        recursive: matches.is_present(OPT_RECURSIVE) || matches.is_present(OPT_R_UPPER),
        dir: matches.is_present(OPT_DIR),
        verbose: matches.is_present(OPT_VERBOSE),
        passthrough: matches.is_present(OPT_PASSTHROUGH),
        shred: matches.is_present(OPT_SHRED),
        undo: matches.is_present(OPT_UNDO),
        empty: matches.is_present(OPT_EMPTY),
        list: matches.is_present(OPT_LIST),
        system: matches.is_present(OPT_SYSTEM),
        moove: matches.is_present(OPT_MOOVE),
        dest_dir: {
            if matches.is_present(OPT_MOOVE) {
                matches.value_of(OPT_MOOVE).unwrap_or(&String::new()).to_string()
            } else {
                if matches.is_present(OPT_SYSTEM) {
                    "/.Trash".to_string()
                } else {
                    let home = std::env::var("HOME").unwrap();
                    [ home, "/.local/share/Trash".to_string() ].concat()
                }
            }
        },

        ovum: matches.is_present(OPT_OVUM),

        files: matches
            .values_of(ARG_FILES)
            .map(|v| v.map(ToString::to_string).collect())
            .unwrap_or_default(),
    };

    options
}

pub fn parse_mut_options() -> MutOptions {
    let mut mut_options = MutOptions {
        dest_dir: String::new(),
        cwd: String::new(),
        recur_dest: String::new(),
    };

    mut_options
}

