use std::path::Path;
use std::fs;
use std::io::Write;
use std::time::SystemTime;

use crate::parse;

use regex::Regex;

static SECS_IN_YEAR: u64   = 31556926;
static SECS_IN_MONTH: u64  = 2629743;
static SECS_IN_DAY: u64    = 86400;
static SECS_IN_HOUR: u64   = 3600;
static SECS_IN_MINUTE: u64 = 60;
static SECS_IN_SECOND: u64 = 1;

pub struct TrashInfo {
    pub path: String,
    pub ddate: String,
}

pub fn make_trashinfo(src_file: &Path, dest_file: &Path, trashinfo_dir: &Path, options: &parse::Options) -> bool {
    if src_file.is_dir() {
        let size_str = match get_size(src_file, options) {
            Ok(s)   => { if options.verbose { println!("...creating trashinfo for {}: got dir size for {0}", src_file.display()) }; s},
            Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed to get dir size for {0} -- {:?}", src_file.display(), e.kind()) }; return true },
        };
        let dir_dest = &trashinfo_dir.parent().unwrap().join("directorysizes");

        let mut file = match fs::OpenOptions::new()
            .write(true)
            .append(true)
            .open(&dir_dest) {
            Ok(f)   => { if options.verbose { println!("...creating trashinfo for {}: opened {} for writing", src_file.display(), dir_dest.display()) }; f },
            Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed to open {} file for writing -- {:?}", src_file.display(), dir_dest.display(), e.kind()) }; return true },
            };

        write!(file, "{}", size_str).unwrap();
    }
    
    let path_str  = match get_path(src_file, options) {
        Ok(s)   => { if options.verbose { println!("...creating trashinfo for {}: got path information", src_file.display()) }; s},
        Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed to get path information -- {:?}", src_file.display(), e.kind()) }; return true },
    };

    let ddate_str = match get_ddate(options) {
        Ok(s)   => { if options.verbose { println!("...creating trashinfo for {}: got deletion date information", src_file.display()) }; s},
        Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed to get deletion date information -- {:?}", src_file.display(), e.duration()) }; return true },
    };

    let content_str = format!("[Trash Info]\nPath={}\nDeletionDate={}\n", path_str, ddate_str);
    let content = content_str.as_bytes();

    let mut trashinfo_file = dest_file.file_name().unwrap().to_str().unwrap().to_string();
    trashinfo_file.retain(|c| c != '/');
    trashinfo_file.push_str(".trashinfo");

    let dest = &trashinfo_dir.join(&trashinfo_file);

    let mut writable_file = match fs::File::create(&dest) {
        Ok(f)   => { if options.verbose { println!("...creating trashinfo for {}: created trashinfo file {}", src_file.display(), dest.display()) }; f },
        Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed to create trashinfo file {} -- {:?}", src_file.display(), dest.display(), e.kind()) }; return true },
    };

    match writable_file.write_all(&content) {
        Ok(_)   => { if options.verbose { println!("...creating trashinfo for {}: writ trashinfo", src_file.display()) } },
        Err(e)  => { if !options.force { eprintln!("...could not create trashinfo for {}: failed writing trashinfo -- {:?}", src_file.display(), e.kind()) }; return true },
    }

    false
}

pub fn read_trashinfo(file: &Path, trashinfo_dir: &Path) -> std::io::Result<TrashInfo> {

    let trashinfo_file_path = format!("{}/{}.trashinfo", trashinfo_dir.display(), file.display());

    let trashinfo_file: String = match fs::read_to_string(&trashinfo_file_path) {
        Ok(c)   => { c },
        Err(e)  => { return Err(e) },
    };

    
    let trashinfo_file_lines: Vec<&str> = trashinfo_file.lines().collect();

    let trashinfo_path  = trashinfo_file_lines[1].to_string().strip_prefix("Path=").unwrap().to_string();
    let trashinfo_ddate = trashinfo_file_lines[2].to_string().strip_prefix("DeletionDate=").unwrap().to_string();


    Ok(TrashInfo {
        path: trashinfo_path,
        ddate: trashinfo_ddate,
    })
}

fn get_path(file: &Path, options: &parse::Options) -> std::io::Result<String> {
    let canon_file = match fs::canonicalize(file) {
        Ok(v)   => {v},
        Err(e)  => {return Err(e)},
    };

    let canon_path = canon_file.as_path();
    let canon_str  = canon_path.display().to_string();

    Ok(canon_str)
}

fn get_ddate(options: &parse::Options) -> std::result::Result<String, std::time::SystemTimeError> {
    let now = SystemTime::now();
    let since_unix = match now.duration_since(SystemTime::UNIX_EPOCH) {
        Ok(t)   => {t},
        Err(e)  => {return Err(e)},
    };

    let mut now_remainder = since_unix.as_secs();
    
    let now_year   = 1970 + (now_remainder / SECS_IN_YEAR);
    now_remainder  = now_remainder % SECS_IN_YEAR;

    let now_month  = 1 + (now_remainder / SECS_IN_MONTH);
    now_remainder  = now_remainder % SECS_IN_MONTH;
    
    let now_day    = 1 + (now_remainder / SECS_IN_DAY);
    now_remainder  = now_remainder % SECS_IN_DAY;

    let now_hour   = now_remainder / SECS_IN_HOUR;
    now_remainder  = now_remainder % SECS_IN_HOUR;

    let now_minute = now_remainder / SECS_IN_MINUTE;
    now_remainder  = now_remainder % SECS_IN_MINUTE;

    let now_second = now_remainder / SECS_IN_SECOND;
    now_remainder  = now_remainder % SECS_IN_SECOND;

    let time = format!(
        "{}-{:02}-{:02}T{:02}:{:02}:{:02}",
        now_year,
        now_month,
        now_day,
        now_hour,
        now_minute,
        now_second
        );

    Ok(time)
}

fn get_size(dir: &Path, options: &parse::Options) -> std::io::Result<String> {
    let size = match fs::metadata(dir) {
        Ok(l)   => l.len(),
        Err(e)  => { return Err(e) },
    };
    let file_name = dir.file_name().unwrap().to_str().unwrap();
    let size_str = format!("{}\t{}\n", file_name, size);

    Ok(size_str)
}
